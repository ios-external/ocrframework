// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6 (swiftlang-5.6.0.323.62 clang-1316.0.20.8)
// swift-module-flags: -target arm64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name OkycOCR
import AVFoundation
import CropViewController
import Foundation
@_exported import OkycOCR
import Swift
import UIKit
import _Concurrency
public enum DOCUMENT_TYPE : Swift.String {
  case PAN_CARD
  case AADHAAR
  case DRIVING_LICENSE
  case VOTER_ID
  case PASSPORT
  case AADHAAR_LETTER
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum DOCUMENT_FACE {
  case FRONT
  case BACK
  public static func == (a: OkycOCR.DOCUMENT_FACE, b: OkycOCR.DOCUMENT_FACE) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum OCR_STATE {
  case CAPTURING
  case CONFIRMING
  public static func == (a: OkycOCR.OCR_STATE, b: OkycOCR.OCR_STATE) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol OCRDelegate : UIKit.UIViewController {
  func onResponse(resp: [Swift.String : Any], captureBackFace: Swift.Bool, success: Swift.Bool)
  func onStateChange(currentState: OkycOCR.OCR_STATE)
  func toggleLoader(show: Swift.Bool, completion: (() -> Swift.Void)?)
}
@objc @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) public class OCRViewController : UIKit.UIViewController {
  @_Concurrency.MainActor(unsafe) public init(docType: OkycOCR.DOCUMENT_TYPE, delegate: OkycOCR.OCRDelegate, karzaToken: Swift.String, environment: Swift.String, aadhaarMasking: Swift.Bool = false)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidLoad()
  @_Concurrency.MainActor(unsafe) public func capturePhoto()
  @_Concurrency.MainActor(unsafe) public func retakePhoto()
  @_Concurrency.MainActor(unsafe) public func confirmPhoto()
  @objc deinit
}
extension OkycOCR.OCRViewController : AVFoundation.AVCapturePhotoCaptureDelegate, CropViewController.CropViewControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func photoOutput(_ output: AVFoundation.AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVFoundation.AVCapturePhoto, error: Swift.Error?)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func cropViewController(_ cropViewController: CropViewController.SwiftCropViewController, didCropToImage image: UIKit.UIImage, withRect cropRect: CoreGraphics.CGRect, angle: Swift.Int)
}
extension OkycOCR.DOCUMENT_TYPE : Swift.Equatable {}
extension OkycOCR.DOCUMENT_TYPE : Swift.Hashable {}
extension OkycOCR.DOCUMENT_TYPE : Swift.RawRepresentable {}
extension OkycOCR.DOCUMENT_FACE : Swift.Equatable {}
extension OkycOCR.DOCUMENT_FACE : Swift.Hashable {}
extension OkycOCR.OCR_STATE : Swift.Equatable {}
extension OkycOCR.OCR_STATE : Swift.Hashable {}
